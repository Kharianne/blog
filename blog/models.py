from django.db import models
from django.utils import timezone
from django.urls import reverse
from django.template.defaultfilters import slugify
from markdownx.models import MarkdownxField
from markdownx.utils import markdownify


class Category(models.Model):

    class Meta:
        verbose_name_plural = "categories"

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    slug = models.SlugField(editable=False)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('category-list', args=[self.slug])

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        super(Category, self).save(*args, **kwargs)


class Post(models.Model):
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    title = models.CharField(max_length=300)
    text = MarkdownxField()
    created_date = models.DateTimeField(
            default=timezone.now)
    published_date = models.DateTimeField(
            blank=True, null=True)
    category = models.ForeignKey('Category', on_delete=models.SET_DEFAULT, default=0)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    @property
    def formatted_markdown(self):
        return markdownify(self.text)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('detail', args=[str(self.pk)])


class Quote(models.Model):
    author = models.CharField(max_length=200)
    text = models.TextField()

    def __str__(self):
        return self.text, self.author


class SocialMedia(models.Model):
    class Meta:
        verbose_name_plural = "social media"

    name = models.CharField(max_length=50)
    url = models.URLField()
    type = models.CharField(max_length=100)

    def __str__(self):
        return self.name






