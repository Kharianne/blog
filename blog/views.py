from django.shortcuts import render
from django.views.generic import TemplateView, ListView, DetailView
from .models import Category
from .models import Post, SocialMedia


class BaseList(ListView):
    model = Post
    context_object_name = 'post_list'
    template_name = 'blog/post_list.html'


# PostList set as HomePage view
class HomePage(BaseList):

    def get(self, request, *args, **kwargs):
        query = request.GET.get('q')
        if query:
            self.queryset = Post.objects.filter(text__icontains=query)
        else:
            self.queryset = Post.objects.order_by('-published_date')[:5]
        return super(HomePage, self).get(request, *args, **kwargs)


class CategoryList(BaseList):

    def get(self, request, *args, **kwargs):
        slug = kwargs.get('slug')
        slug_id = Category.objects.get(slug=slug)
        self.queryset = Post.objects.filter(category=slug_id).order_by\
            ('-published_date')
        return super(CategoryList, self).get(request, *args, **kwargs)


class PostDetail(DetailView):
    model = Post
    context_object_name = 'post'
    template_name = 'blog/post_detail.html'


def categories(request):
    category_list = Category.objects.all()
    return {'category_list': category_list}


def social_media(request):
    media_list = SocialMedia.objects.all()
    return {'media_list': media_list}


def side_list(request):
    post_side_list = Post.objects.order_by('-published_date')[:10]
    return {'side_list': post_side_list}


