from django.forms import ModelForm, ChoiceField
from .models import SocialMedia


def get_my_choices():
    choices = (
            ('FB', 'facebook'),
            ('TW', 'twitter'),
            ('GH', 'github')
        )
    choices_to_render = list()
    # Not to ping database in every iteration
    queryset = SocialMedia.objects.all()
    for choice in choices:
        if not queryset.filter(type=choice[0]):
            choices_to_render.append(choice)
    return choices_to_render


class SocialMedialForm(ModelForm):
    class Meta:
        model = SocialMedia
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super(SocialMedialForm, self).__init__(*args, **kwargs)
        self.fields['type'] = ChoiceField(choices=get_my_choices())