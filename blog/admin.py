from django.contrib import admin
from .models import Post
from .models import Category
from .models import Quote
from .models import SocialMedia
from .custom_admin import SocialMediaAdmin
from markdownx.admin import MarkdownxModelAdmin


admin.site.register(Post, MarkdownxModelAdmin)
admin.site.register(Category)
admin.site.register(Quote)
admin.site.register(SocialMedia, SocialMediaAdmin)
