from django.contrib import admin
from .forms import SocialMedialForm


class SocialMediaAdmin(admin.ModelAdmin):
    form = SocialMedialForm
